const routesLoggedOut = [
  { name: 'Home', component: 'home-page' },
  { name: 'About' },
  { name: 'Contact' },
  { name: 'Login', component: 'login-page', hidden: true }
];

const routesLoggedIn = [
  { name: 'Home', component: 'home-page' },
  { name: 'Accounts', component: 'accounts-page' },
  { name: 'About' },
  { name: 'Contact' },
  { name: 'Login', component: 'login-page', hidden: true },
  { name: 'Operations', component: 'operations-page', hidden: true },
  { name: 'Payments', component: 'payments-page', hidden: true, cache: false },
];