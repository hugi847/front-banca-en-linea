var express = require('express'),
  path = require('path'),
  app = express(),
  port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/build/default'));
app.listen(port);

console.log(`Front Banca en linea inicializado, escuchando en puerto ${port}`);

app.get('/', (req, res) =>
  res.sendFile('index.html', {
    root: '.'
  })
);
